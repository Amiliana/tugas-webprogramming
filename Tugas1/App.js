import React from "react";
import { ImageBackground, StyleSheet, Text, View } from "react-native";

const image = { uri: "https://reactjs.org/logo-og.png" };

const App = () => (
  <View style={styles.container}>
    <ImageBackground source={image} style={styles.image}> 
    <Text style={styles.bold}>Welcome To My Blog</Text>
    <Text style={styles.bigColor}>Dewi Amiliana</Text>
    </ImageBackground>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column"
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  bold: {
    fontSize: 25,
    paddingBottom: 700,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white'
  },
  bigColor: {
    fontSize: 18,
    paddingTop: 10,
    textAlign: 'center',
    color: 'white',
  }
});

export default App;