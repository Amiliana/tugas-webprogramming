import React, {Component} from 'react';
import {View, StyleSheet, Image, ScrollView} from 'react-native';

class FlexBox extends Component{
    render() {
        return(
            <View style= {styles.container}>
                <View style = {styles.NumberOne}>
                    <Image source={require('/Users/User/apaaja/src/assets/gmbr1.jpg')} style={styles.gambar}/>
                </View>
                <View style = {styles.NumberTwo}>
                    <ScrollView>
                         <Image source={require('/Users/User/apaaja/src/assets/story1.jpg')} style={styles.story}/>   
                         <Image source={require('/Users/User/apaaja/src/assets/gambar2.jpg')} style={styles.Img}/>
                         <Image source={require('/Users/User/apaaja/src/assets/gambar3.jpg')} style={styles.konten1}/>
                         <Image source={require('/Users/User/apaaja/src/assets/gambar4.jpg')} style={styles.konten2}/>
                         <Image source={require('/Users/User/apaaja/src/assets/gambar6.jpg')} style={styles.konten2}/>
                         <Image source={require('/Users/User/apaaja/src/assets/gambar7.jpg')} style={styles.konten2}/>
                         <Image source={require('/Users/User/apaaja/src/assets/gambar8.jpg')} style={styles.konten2}/>
                         <Image source={require('/Users/User/apaaja/src/assets/gambar9.jpg')} style={styles.konten2}/>
                    </ScrollView>    
                </View>
                <View style = {styles.NumberThree}>
                     <Image source={require('/Users/User/apaaja/src/assets/gambar5.jpg')} style={styles.bawah}/>
                </View>
            </View>  
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        backgroundColor : 'red'
    },
    NumberOne :{
        flex : 0,
        backgroundColor : 'white'
    },
    NumberTwo :{
        flex : 10,
        backgroundColor : 'white'
    },
    NumberThree :{
        flex : 1,
        backgroundColor : 'white'
    },
    gambar : {
        height: 55,
        width: 425
    },
    story : {
        height : 110,
        width: 410
    },
    Img : {
        height: 650,
        width: 425
    },
    konten1 : {
        height: 590,
        width: 425
    },
    konten2 : {
        height: 590,
        width: 425
    },
    bawah : {
        height : 52,
        width : 430
    }


});

export default FlexBox;